var deckOfCard = function(){

    var suit = ["spade","diamond","heart","club"];
    var name = ["Ace","2 ","3 ","4","5","6","7","8","9","10","Jack","Queen","King"];
    var value = [1,2,3,4,5,6,7,8,9,10,10,10,10];
    var theDeck =[];

// imgArray[0] = new Image();
// imgArray[0].src = 'images/img/Splash_image1.jpg';
    for(var s in suit){ //suit
        for(var n in name){ //name
            theDeck.push({
                suit:suit[s],
                name:name[n],
                value:value[n],//value
                image:image[n]
            });
        }
    }
    var shff = shff || 3;
    //var count = 0;
    for(var times=0 ; times < shff ; times++){
        for(var i = 0 ; i < theDeck.length ; i++){
            var dst = parseInt(Math.random() * theDeck.length);
            var swapVal = theDeck[i];
            theDeck[i] = theDeck[dst];
            theDeck[dst] = swapVal;
        }
    }
    return (theDeck);
}


//Loading libraries
var path = require("path");
var express = require("express");

//instance of express
var app = express();

app.get("/employee/:empNo",function(req,resp){
var empNo = req.params.empNo;

resp.type("application/json");
resp.status(200);
resp.json({
    name:"F",
    email:"f@g.com",
    empNo:empNo

    });
});

//json 
app.get("/card/:cardNo",function(req,resp){
var cardNo = req.params.cardNo;

// check is its a number
if(isNaN(cardNo) ||cardNo > 52 ) {
    resp.type("text/html");
    resp.status(200);
    resp.send( "<h1>Bad Request </h1>" );
    
}

var taitee = deckOfCard();
var requestedCards =  taitee.slice(0,cardNo);

resp.type("application/json");
resp.status(200);
resp.send( requestedCards );
});

//json obj
app.get("/cards",function(req,resp){
console.log("inside json");
var taitee = deckOfCard();
resp.type("application/json");
resp.status(200);
resp.send( taitee );
});

//setting up port
var port = parseInt(process.argv[2])|| parseInt(process.env.APP_PORT) ||3000;
app.set("port", port );

//start up the server on the sepcified port 
app.listen(app.get("port"), function(){
console.info("Application is listening on port : "+ app.get("port"));
});
